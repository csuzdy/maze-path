"""
Find the path in maze.
"""
import doctest
import time
import textmap
import pdb
import pyperclip
import networkx as nx

# ls *.py | entr -p -c python3 /_

def get_start(arrmap):
    '''Get start element 's'
    >>> arrmap = [['#', '#', '#', '#', '#'],
    ...           ['#', 's', '#', ' ', '#'],
    ...           ['#', ' ', ' ', ' ', '#'],
    ...           ['#', '#', '#', '#', '#']]
    >>> get_start(arrmap)
    (1, 1)
    '''
    for y, row in enumerate(arrmap):
        for x, cell in enumerate(row):
            if cell == 's':
                return (x, y)
    raise ValueError('No start found')


def get_coins(arrmap):
    '''Get start element 's'
    >>> arrmap = [['#', '#', '#', '#', '#'],
    ...           ['#', 'c', '#', ' ', '#'],
    ...           ['#', ' ', ' ', 'c', '#'],
    ...           ['#', '#', '#', '#', '#']]
    >>> get_coins(arrmap)
    [(1, 1), (3, 2)]
    '''
    coins = []
    for y, row in enumerate(arrmap):
        for x, cell in enumerate(row):
            if cell == 'c':
                coins.append((x, y))
    return coins


def construct_path(end, came_from):
    '''Reconstruct the path using the came_from map
    >>> construct_path((1, 1), {(1, 1): (2, 1), (2, 1): (0, 0)})
    [(0, 0), (2, 1), (1, 1)]
    '''
    act = end
    ret = [act]
    while act in came_from:
        act = came_from[act]
        ret.append(act)
    ret.reverse()
    return ret


def get_direction(act, n):
    '''Get the direction (S, W, E or N) from act to n
    >>> get_direction((0, 0), (1, 0))
    'E'
    >>> get_direction((1, 0), (0, 0))
    'W'
    >>> get_direction((0, 0), (0, 1))
    'S'
    >>> get_direction((0, 1), (0, 0))
    'N'
    '''
    x1, y1 = act
    x2, y2 = n
    xd = x2-x1
    yd = y2-y1
    if xd > 0:
        return 'E'
    elif xd < 0:
        return 'W'
    if yd > 0:
        return 'S'
    elif yd < 0:
        return 'N'


def get_movement(arrmap, start, end):
    '''Decide if movement is F, U or D
    >>> arrmap = [['#', '#', '#', '#', '#'],
    ...           ['#', ' ', '#', ' ', '#'],
    ...           ['#', 'c', ' ', ' ', '#'],
    ...           ['#', '#', '#', '#', '#']]
    >>> get_movement(arrmap, (0, 0), (1, 0))
    'F'
    >>> get_movement(arrmap, (1, 0), (0, 0))
    'F'
    >>> get_movement(arrmap, (1, 0), (1, 1))
    'D'
    >>> get_movement(arrmap, (1, 1), (1, 0))
    'U'
    >>> get_movement(arrmap, (1, 2), (1, 3))
    'U'
    >>> get_movement(arrmap, (1, 3), (1, 2))
    'D'
    >>> get_movement(arrmap, (1, 2), (2, 2))
    'F'
    '''
    x1, y1 = start
    x2, y2 = end
    cell1 = arrmap[y1][x1]
    cell2 = arrmap[y2][x2]
    if cell1 == cell2:
        return 'F'
    elif cell1 == '#' and cell2 != '#':
        return 'D'
    elif cell1 != '#' and cell2 == '#':
        return 'U'
    else:
        return 'F'


def construct_moves(path, arrmap, startdir='N'):
    '''Return movements from path
    >>> arrmap = [['#', '#', '#', '#', '#'],
    ...           ['#', ' ', '#', ' ', '#'],
    ...           ['#', ' ', ' ', ' ', '#'],
    ...           ['#', '#', '#', '#', '#']]
    >>> path = [(1, 1), (1, 2), (2, 2), (3, 2), (3, 1)]
    >>> construct_moves(path, arrmap)
    ('SFEFFNF', 'N')
    '''
    direction = startdir
    ret = ''
    for i in range(len(path)-1):
        act = path[i]
        n = path[i+1]
        ndir = get_direction(act, n)
        if ndir and ndir != direction:
            ret += ndir
            direction = ndir
        forward = get_movement(arrmap, act, n)
        ret += forward
    return ret, direction


def bfs(arrmap, start, end):
    '''Breadth-first search
    >>> arrmap = [['#', '#', '#', '#', '#'],
    ...           ['#', ' ', '#', ' ', '#'],
    ...           ['#', ' ', ' ', ' ', '#'],
    ...           ['#', '#', '#', '#', '#']]
    >>> bfs(arrmap, (1, 1), (3, 1))
    [(1, 1), (2, 1), (3, 1)]
    '''
    open_set = [start]
    closed_set = set()
    came_from = {}

    while open_set:
        act = open_set.pop()
        if act == end:
            return construct_path(act, came_from)

        for neighbour in textmap.get_neighbours(act, arrmap):
            if neighbour in closed_set:
                continue
            if neighbour not in open_set:
                came_from[neighbour] = act
                open_set.insert(0, neighbour)
        closed_set.add(act)


def distance(start, end):
    '''Distance heuristic
    >>> distance((0, 0), (0, 0))
    0
    >>> distance((5, 0), (0, 0))
    5
    >>> distance((5, 5), (0, 0))
    10
    '''
    return abs(start[0]-end[0]) + abs(start[1]-end[1])


def get_lowest_fscore(openset, fscore):
    minval = None
    node = None
    for val in openset:
        v = fscore.get(val, 9999999999)
        if not minval or v < minval:
            minval = v
            node = val
    return node


def astar(arrmap, start, end):
    '''A* search
    >>> arrmap = [['#', '#', '#', '#', '#'],
    ...           ['#', ' ', '#', ' ', '#'],
    ...           ['#', ' ', ' ', ' ', '#'],
    ...           ['#', '#', '#', '#', '#']]
    >>> bfs(arrmap, (1, 1), (3, 1))
    [(1, 1), (2, 1), (3, 1)]
    '''
    open_set = [start]
    closed_set = set()
    came_from = {}
    gscore = {}
    gscore[start] = 0
    fscore = {}
    fscore[start] = distance(start, end)

    while open_set:
        act = get_lowest_fscore(open_set, fscore)
        if act == end:
            return construct_path(act, came_from)

        open_set.remove(act)
        closed_set.add(act)

        for neighbour in textmap.get_neighbours(act, arrmap):
            if neighbour in closed_set:
                continue
            if neighbour not in open_set:
                open_set.append(neighbour)

            plus = 0
            if act in came_from and get_direction(act, neighbour) != get_direction(came_from[act], act):
                plus = 1

            tentative_gscore = gscore[act] + textmap.get_weight(arrmap, act, neighbour) + plus
            if tentative_gscore >= gscore.get(neighbour, 999999999):
                continue

            came_from[neighbour] = act
            gscore[neighbour] = tentative_gscore
            fscore[neighbour] = gscore[neighbour] + distance(neighbour, end)


def shortestmove(arrmap, graph, start, lastdir, end):
    #path = nx.shortest_path(graph, start, end, weight='weight')
    path = astar(arrmap, start, end)
    return construct_moves(path, arrmap, startdir=lastdir)


def plen(path):
    '''Get the cost of movements
    >>> plen('U')
    8
    >>> plen('D')
    8
    >>> plen('F')
    1
    >>> plen('FUD')
    17
    '''
    return len(path.replace('U', 'U'*8).replace('D', 'D'*8))


def getclosest(start, lastdir, coins, arrmap, graph):
    minpath = None
    mincoin = None
    minlastdir = None
    for coin in coins:
        path, d = shortestmove(arrmap, graph, start, lastdir, coin)
        if not minpath or (plen(path) < plen(minpath)):
            minpath = path
            mincoin = coin
            minlastdir = d
    return (mincoin, minpath, minlastdir)


def solve(arrmap):
    solution = ''
    start = get_start(arrmap)
    coins = get_coins(arrmap)
    lastdir = 'N'
    graph = textmap.arrmap_to_graph(arrmap)
    while len(coins) > 0:
        coin, path, lastdir = getclosest(start, lastdir, coins, arrmap, graph)
        solution += path
        coins.remove(coin)
        start = coin
    return solution


def main():
    """Main function to test the whole functionality"""
    arrmap = textmap.read_map('mazes/level5.in')
    res = solve(arrmap)
    if not res:
        print('Not found')
    else:
        print(res)
        pyperclip.copy(res)


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=False)
