"""
Draw the maze and metadata with matplotlib
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection

def init():
    fig, ax = plt.subplots()
    plt.xlim((0, 1))
    plt.ylim((0, 1))
    return fig


def update(ax, arrmap):
    ax.clear()
    width = len(arrmap[0])
    height = len(arrmap)
    w_step = 1/width
    h_step = 1/height
    print('wstep: %f, hstep: %f' % (w_step, h_step))
    grid = np.mgrid[0:1:width*1j,0:1:height*1j].reshape(2, -1).T
    # print(grid)
    patches = [mpatches.Rectangle(cell, w_step, h_step) for cell in grid]
    patch_collection = PatchCollection(patches)
    ax.add_collection(patch_collection)


fig = init()
ax = fig.axes[0]

update(ax, arrmap)
