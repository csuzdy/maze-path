from copy import deepcopy
import networkx as nx

def map_to_array(textmap):
    ''' Returns the 2d array version of the map
    >>> textmap = """
    ... ####
    ... #..#
    ... ####
    ... """
    >>> map_to_array(textmap)
    [['#', '#', '#', '#'], ['#', '.', '.', '#'], ['#', '#', '#', '#']]
    '''
    return [list(row) for row in textmap.split('\n') if row]


def arrmap_to_graph(arrmap):
    '''Convert arrmap to networkX graph
    >>> arrmap = [['#', '#', '#'],
    ...           ['#', '.', '#'],
    ...           ['#', '#', '#']]
    >>> G = arrmap_to_graph(arrmap)
    >>> G.number_of_nodes()
    9
    >>> G.number_of_edges()
    12
    >>> G[(1, 1)][(0, 1)]
    {'weight': 8}
    >>> G[(0, 0)][(0, 1)]
    {'weight': 1}
    >>> import networkx as nx
    >>> nx.shortest_path(G, (0, 1), (2, 1), weight='weight') 
    [(0, 1), (0, 0), (1, 0), (2, 0), (2, 1)]
    '''
    G = nx.Graph()
    width = len(arrmap[0])
    height = len(arrmap)
    for x in range(width):
        for y in range(height):
            G.add_node((x, y))
    for x in range(width):
        for y in range(height):
            nord = (x, y-1)
            south = (x, y+1)
            west = (x-1, y)
            east = (x+1, y)
            act = (x, y)
            for coord in (nord, south, west, east):
                if is_accessible(coord, arrmap):
                    weight = get_weight(arrmap, act, coord)
                    G.add_edge(act, coord, weight=weight)
    return G


def draw_map(arrmap, path):
    '''Draw the constructed path to the text map with dots
    >>> arrmap = [['#', '#', '#', '#', '#'],
    ...           ['#', '.', '#', '.', '#'],
    ...           ['#', '.', '.', '.', '#'],
    ...           ['#', '#', '#', '#', '#']]
    >>> draw_map(arrmap, [(1, 1), (1, 2), (2, 2), (3, 2), (3, 1)])
    [['#', '#', '#', '#', '#'], ['#', '*', '#', '*', '#'], ['#', '*', '*', '*', '#'], ['#', '#', '#', '#', '#']]
    '''
    m = deepcopy(arrmap)
    for step in path:
        x,y = step
        m[y][x] = "*"
    return m


def to_textmap(arrmap):
    '''Convert the arrmap to multiline string
    >>> arrmap = [['#', '#', '#', '#', '#'],
    ...           ['#', '.', '#', '.', '#'],
    ...           ['#', '.', '.', '.', '#'],
    ...           ['#', '#', '#', '#', '#']]
    >>> to_textmap(arrmap)
    '#####\\n#.#.#\\n#...#\\n#####\\n'
    '''
    ret = ''
    for row in arrmap:
        ret += "".join(row) + '\n'
    return ret


def read_map(filename):
    '''Opens file and read it contents
    >>> ret = read_map('mazes/example1.in')
    >>> ["".join(row) for row in ret]
    ['s.......', '#######.', '.c...#..', '.....c..']
    '''
    with open(filename) as f:
        f.readline()
        maze_map = f.read()
        arrmap = map_to_array(maze_map)
        return arrmap


def is_accessible(coord, arrmap):
    '''Return True if coord is not out of bounds and there is not a wall.
    >>> arrmap = [['#', '#', '#'], ['#', '.', '#'], ['#', '#', '#']]
    >>> is_accessible((0, 0), arrmap)
    True
    >>> is_accessible((1, 1), arrmap)
    True
    >>> arrmap = [[]]
    >>> is_accessible((-1, 0), arrmap)
    False
    >>> is_accessible((-1, -1), arrmap)
    False
    >>> is_accessible((0, 1), arrmap)
    False
    >>> is_accessible((1, 1), arrmap)
    False
    '''
    x,y = coord
    if x < 0 or y < 0:
        return False
    if y >= len(arrmap):
        return False
    if x >= len(arrmap[0]):
        return False
    return True


def get_neighbours(coord, arrmap):
    '''Return list of neighbour coordinates
    >>> arrmap = [['#', '#', '#', '#', '#'],
    ...           ['#', '.', '.', '.', '#'],
    ...           ['#', '.', '.', '.', '#'],
    ...           ['#', '.', '.', '.', '#'],
    ...           ['#', '#', '#', '#', '#']]
    >>> get_neighbours((0, 0), arrmap)
    [(1, 0), (0, 1)]
    >>> get_neighbours((1, 1), arrmap)
    [(0, 1), (2, 1), (1, 0), (1, 2)]
    >>> get_neighbours((2, 2), arrmap)
    [(1, 2), (3, 2), (2, 1), (2, 3)]
    '''
    x,y = coord
    if not is_accessible(coord, arrmap):
        return []
    ret = [(x-1, y), (x+1,y), (x, y-1), (x, y+1)]
    return list(filter(lambda c: is_accessible(c, arrmap), ret))


def get_weight(arrmap, start, end):
    '''Get the weight of an edge (1 or 8)
    >>> arrmap = [['#', '#', '#', '#', '#'],
    ...           ['#', '.', '.', '.', '#'],
    ...           ['#', '.', '.', '.', '#'],
    ...           ['#', '.', '.', '.', '#'],
    ...           ['#', '#', '#', '#', '#']]
    >>> get_weight(arrmap, (0, 0), (0, 1))
    1
    >>> get_weight(arrmap, (1, 1), (0, 1))
    8
    '''
    x1, y1 = start
    x2, y2 = end
    cell1 = arrmap[y1][x1]
    cell2 = arrmap[y2][x2]
    if cell1 == cell2:
        return 1
    else:
        return 8


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=False)
